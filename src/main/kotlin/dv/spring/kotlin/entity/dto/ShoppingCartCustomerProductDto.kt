package dv.spring.kotlin.entity.dto

data class ShoppingCartCustomerProductDto(var customerName: DisplayCustomer? = null,
                                          var selectedProduct: List<DisplaySelectedProduct>? = null)

data class DisplayCustomer(var customerName:String? = null)

data class DisplaySelectedProduct (var productname: List<DisplayProduct>? = null,
                                   var quantity: Int? = null)

data class DisplayProduct(var productName: String? = null)

