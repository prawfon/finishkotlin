package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart (var shoppingCartStatus:ShoppingCartStatus? = ShoppingCartStatus.WAIT) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectedProducts = mutableListOf<SelectedProduct>()

    @OneToOne
    var customer: Customer? = null


    @OneToOne
    var shippingAddress: Address? = null
}
