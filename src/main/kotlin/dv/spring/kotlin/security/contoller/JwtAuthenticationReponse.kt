package dv.spring.kotlin.security.contoller

data class JwtAuthenticationResponse(
        var token: String? = null
)