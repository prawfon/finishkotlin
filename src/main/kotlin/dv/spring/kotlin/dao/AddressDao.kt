package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.Manufacturer
import org.springframework.beans.factory.annotation.Autowired

interface AddressDao{
    fun save(address: Address): Address
    fun findById(AddressId: Long): Address?

}